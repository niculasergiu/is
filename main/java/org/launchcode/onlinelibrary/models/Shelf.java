package org.launchcode.onlinelibrary.models;



import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Shelf {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    @Size(min=3, max=15)
    private String name;

    @ManyToMany
    private List<Book> books;

    public Shelf() {}


    public void addItem(Book item) {
        books.add(item);
    }

    public void removeItem(Book item) {
        books.remove(item);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public List<Book> getBooks() {
        return books;
    }


}