package org.launchcode.onlinelibrary.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
@Entity
public class Role {
    @Id
    @GeneratedValue
    private int id;

    @NotNull
    @Size(min=3, max=15)
    private String name;

    @OneToMany
    @JoinColumn(name = "user_id")
    private List<User> users;

    @ManyToMany(mappedBy = "roles")
    private List<TaskUser> taskUsers;


    public void addItem(TaskUser item) {
        taskUsers.add(item);
    }

    public void removeItem(TaskUser item) {
        taskUsers.remove(item);
    }

    public Role(String name) {
        this.name = name;
    }
    public Role(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<TaskUser> getTaskUsers() {
        return taskUsers;
    }

    public void setTaskUsers(List<TaskUser> taskUsers) {
        this.taskUsers = taskUsers;
    }


}