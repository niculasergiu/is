package org.launchcode.onlinelibrary.models.data;

import org.launchcode.onlinelibrary.models.Role;
import org.launchcode.onlinelibrary.models.Shelf;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface RoleDao extends CrudRepository<Role, Integer> {
}
