package org.launchcode.onlinelibrary.models.data;

import org.launchcode.onlinelibrary.models.Shelf;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ShelfDao extends CrudRepository<Shelf, Integer> {
}
