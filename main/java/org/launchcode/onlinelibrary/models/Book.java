package org.launchcode.onlinelibrary.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Book {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    @Size(min=3, max=15)
    private String name;

    @NotNull
    @Size(min=8, max=20)
    private String author;

    @NotNull
    @Size(min=4, max=4, message ="Year fail")
    private String year;


    @NotNull
    @Size(min=1, message ="Description must not be empty")
    private String description;

    @ManyToMany(mappedBy = "books")
    private List<Shelf> shelves;

    @ManyToOne
    private Category category;

    public Book(String name, String author, String year, String description) {
        this.name = name;
        this.author = author;
        this.year = year;
        this.description = description;
    }
    public Book (){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Shelf> getMenus() {
        return shelves;
    }

    public void setMenus(List<Shelf> shelves) {
        this.shelves = shelves;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
