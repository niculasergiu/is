package org.launchcode.onlinelibrary.models.forms;

import org.launchcode.onlinelibrary.models.Book;
import org.launchcode.onlinelibrary.models.Shelf;


import javax.validation.constraints.NotNull;

public class AddShelfItemForm {

    @NotNull
    private int shelfId;

    @NotNull
    private int bookId;


    private Iterable<Book> books;

    private Shelf shelf;

    public AddShelfItemForm() {}

    public AddShelfItemForm(Iterable<Book> books, Shelf shelf) {
        this.books = books;
        this.shelf = shelf;
    }

    public int getShelfId() {
        return shelfId;
    }

    public void setShelfId(int shelfId) {
        this.shelfId = shelfId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public Iterable<Book> getBooks() {
        return books;
    }

    public void setBooks(Iterable<Book> books) {
        this.books = books;
    }

    public Shelf getShelf() {
        return shelf;
    }

    public void setShelf(Shelf shelf) {
        this.shelf = shelf;
    }
}
