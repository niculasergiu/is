package org.launchcode.onlinelibrary.models.forms;

import org.launchcode.onlinelibrary.models.Book;
import org.launchcode.onlinelibrary.models.Role;
import org.launchcode.onlinelibrary.models.Shelf;
import org.launchcode.onlinelibrary.models.TaskUser;


import javax.validation.constraints.NotNull;

public class AddRoleItemForm {

    @NotNull
    private int roleId;

    @NotNull
    private int taskUserId;


    private Iterable<TaskUser> taskUsers;

    private Role role;

    public AddRoleItemForm(Iterable<TaskUser> taskUsers, Role role) {
        this.taskUsers = taskUsers;
        this.role = role;
    }
    public AddRoleItemForm(){}

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getTaskUserId() {
        return taskUserId;
    }

    public void setTaskUserId(int taskUserId) {
        this.taskUserId = taskUserId;
    }

    public Iterable<TaskUser> getTaskUsers() {
        return taskUsers;
    }

    public void setTaskUsers(Iterable<TaskUser> taskUsers) {
        this.taskUsers = taskUsers;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
