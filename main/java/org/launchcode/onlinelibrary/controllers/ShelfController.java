package org.launchcode.onlinelibrary.controllers;

import org.launchcode.onlinelibrary.models.Book;
import org.launchcode.onlinelibrary.models.Category;
import org.launchcode.onlinelibrary.models.Shelf;
import org.launchcode.onlinelibrary.models.data.BookDao;
import org.launchcode.onlinelibrary.models.data.ShelfDao;
import org.launchcode.onlinelibrary.models.forms.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping(value ="shelf")
public class ShelfController {

    @Autowired
    private ShelfDao shelfDao;

    @Autowired
    private BookDao bookDao;

    @RequestMapping(value = "")
    public String index(Model model) {

        model.addAttribute("title", "Shelfs");
        model.addAttribute("shelfs", shelfDao.findAll());

        return "shelf/index";
    }


    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(Model model) {
        model.addAttribute("title", "Add Shelf");
        model.addAttribute(new Shelf());
        return "shelf/add";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String add(Model model, @ModelAttribute @Valid Shelf newShelf,
                      Errors errors) {

        if (errors.hasErrors()) {
            model.addAttribute("title", "Add Shelf");
            return "Shelf/add";
        }
        shelfDao.save(newShelf);
        return "redirect:view/" + newShelf.getId();
    }

    @RequestMapping(value = "view/{shelfId}", method = RequestMethod.GET)
    public String view(Model model, @PathVariable int shelfId) {

        Shelf shelf = shelfDao.findOne(shelfId);

        model.addAttribute("title", shelf.getName());
        model.addAttribute("books", shelf.getBooks());
        model.addAttribute("shelfId", shelf.getId());
        return "shelf/view";
    }



    @RequestMapping(value = "remove-item/{shelfId}", method = RequestMethod.GET)
    public String removeItem(Model model, @PathVariable int shelfId) {

        Shelf shelf = shelfDao.findOne(shelfId);

        AddShelfItemForm form = new AddShelfItemForm(shelf.getBooks(), shelf);

        model.addAttribute("title", "Remove item to shelf: " + shelf.getName());
        model.addAttribute("form", form);

        return "shelf/remove-item";
    }

    @RequestMapping(value = "remove-item", method = RequestMethod.POST)
    public String removeItem(Model model, @ModelAttribute @Valid AddShelfItemForm form,
                             Errors errors) {

        if (errors.hasErrors()) {
            model.addAttribute("form", form);
            return "shelf/remove-item";
        }


        Book theBook = bookDao.findOne(form.getBookId());
        Shelf theShelf = shelfDao.findOne(form.getShelfId());
        theShelf.removeItem(theBook);
        shelfDao.save(theShelf);

        return "redirect:/shelf/view/" + theShelf.getId();
    }


    @RequestMapping(value = "add-item/{shelfId}", method = RequestMethod.GET)
    public String addItem(Model model, @PathVariable int shelfId) {

        Shelf shelf = shelfDao.findOne(shelfId);

        AddShelfItemForm form = new AddShelfItemForm(bookDao.findAll(), shelf);

        model.addAttribute("title", "Add item to shelf: " + shelf.getName());
        model.addAttribute("form", form);

        return "shelf/add-item";
    }

    @RequestMapping(value = "add-item", method = RequestMethod.POST)
    public String addItem(Model model, @ModelAttribute @Valid AddShelfItemForm form,
                          Errors errors) {

        if (errors.hasErrors()) {
            model.addAttribute("form", form);
            return "shelf/add-item";
        }


        Book theBook = bookDao.findOne(form.getBookId());
        Shelf theShelf = shelfDao.findOne(form.getShelfId());
        theShelf.addItem(theBook);
        shelfDao.save(theShelf);

        return "redirect:/shelf/view/" + theShelf.getId();
    }






    @RequestMapping(value = "remove", method = RequestMethod.GET)
    public String displayRemoveBookForm(Model model){
        model.addAttribute("shelfs", shelfDao.findAll());
        model.addAttribute("title","Remove shelf");
        return "shelf/remove";
    }

    @RequestMapping(value = "remove", method = RequestMethod.POST)
    public String processRemoveBookForm(@RequestParam(required = false) int[] ids) {

        if(ids == null){
            return "redirect:/shelf/remove";
        }

        for (int id : ids) {
            shelfDao.delete(id);
        }
        return "redirect:";
    }

    @RequestMapping(value = "edit/{shelfId}", method = RequestMethod.GET)
    public String displayEditShelfForm(Model model, @PathVariable int shelfId) {

        model.addAttribute("title", "Edit Shelf");
        model.addAttribute("shelf",shelfDao.findOne(shelfId));
        model.addAttribute("shelfs", shelfDao.findAll());
        return "shelf/edit";
    }

    @RequestMapping(value = "edit/{shelfId}", method = RequestMethod.POST)
    public String processEditShelfIdForm(Model model, @PathVariable int shelfId,
                                          @ModelAttribute  @Valid Category newShelf,
                                          Errors errors) {

        if (errors.hasErrors()) {
            model.addAttribute("title", "Add Shelf");
            return "shelf/edit";
        }

        Shelf shelf = shelfDao.findOne(shelfId);
        shelf.setName(newShelf.getName());
        shelfDao.save(shelf);

        return "redirect:/shelf";
    }





}
