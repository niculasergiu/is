package org.launchcode.onlinelibrary.controllers;


import org.launchcode.onlinelibrary.models.Book;
import org.launchcode.onlinelibrary.models.Role;
import org.launchcode.onlinelibrary.models.Shelf;
import org.launchcode.onlinelibrary.models.TaskUser;
import org.launchcode.onlinelibrary.models.data.RoleDao;
import org.launchcode.onlinelibrary.models.data.TaskUserDao;
import org.launchcode.onlinelibrary.models.forms.AddRoleItemForm;
import org.launchcode.onlinelibrary.models.forms.AddShelfItemForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("role")
public class RoleController {

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private TaskUserDao taskUserDao;

    @RequestMapping(value = "")
    public String index(Model model) {

        model.addAttribute("roles", roleDao.findAll());
        model.addAttribute("title", "Roles");

        return "role/index";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(Model model) {

        model.addAttribute("title", "Add Role");
        model.addAttribute("role", new Role());
        return "role/add";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String add(Model model, @ModelAttribute @Valid Role role, Errors errors) {

        if (errors.hasErrors()) {
            return "role/add";
        }
        roleDao.save(role);
        return "redirect:";
    }

    @RequestMapping(value = "remove", method = RequestMethod.GET)
    public String displayRemoveRoleForm(Model model){
        model.addAttribute("roles", roleDao.findAll());
        model.addAttribute("title","Remove Role");
        return "role/remove";
    }

    @RequestMapping(value = "remove", method = RequestMethod.POST)
    public String processRemoveRoleForm(@RequestParam(required = false) int[] ids) {

        if(ids == null){
            return "redirect:/role/remove";
        }

        for (int id : ids) {
            roleDao.delete(id);
        }
        return "redirect:";
    }

    @RequestMapping(value = "edit/{roleId}", method = RequestMethod.GET)
    public String displayEditRoleForm(Model model, @PathVariable int roleId) {

        model.addAttribute("title", "Edit Role");
        model.addAttribute("role",roleDao.findOne(roleId));
        model.addAttribute("roles", roleDao.findAll());
        return "role/edit";
    }

    @RequestMapping(value = "edit/{roleId}", method = RequestMethod.POST)
    public String processEditRoleForm(Model model, @PathVariable int roleId,
                                          @ModelAttribute  @Valid Role newRole,
                                          Errors errors) {

        if (errors.hasErrors()) {
            model.addAttribute("title", "Add Role");
            return "role/edit";
        }

        Role role = roleDao.findOne(roleId);
        role.setName(newRole.getName());
        roleDao.save(role);

        return "redirect:/role";
    }

    @RequestMapping(value = "view/{roleId}", method = RequestMethod.GET)
    public String view(Model model, @PathVariable int roleId) {

        Role role = roleDao.findOne(roleId);

        model.addAttribute("title", role.getName());
        model.addAttribute("taskUsers", role.getTaskUsers());
        model.addAttribute("roleId", role.getId());
        return "role/view";
    }



    @RequestMapping(value = "remove-item/{roleId}", method = RequestMethod.GET)
    public String removeItem(Model model, @PathVariable int roleId) {

        Role role = roleDao.findOne(roleId);

        AddRoleItemForm form = new AddRoleItemForm(role.getTaskUsers(), role);

        model.addAttribute("title", "Remove item to role: " + role.getName());
        model.addAttribute("form", form);

        return "role/remove-item";
    }

    @RequestMapping(value = "remove-item", method = RequestMethod.POST)
    public String removeItem(Model model, @ModelAttribute @Valid AddRoleItemForm form,
                             Errors errors) {

        if (errors.hasErrors()) {
            model.addAttribute("form", form);
            return "role/remove-item";
        }


        TaskUser taskUser = taskUserDao.findOne(form.getTaskUserId());
        Role role = roleDao.findOne(form.getRoleId());
        role.removeItem(taskUser);
        roleDao.save(role);

        return "redirect:/role/view/" + role.getId();
    }


    @RequestMapping(value = "add-item/{roleId}", method = RequestMethod.GET)
    public String addItem(Model model, @PathVariable int roleId) {

        Role role = roleDao.findOne(roleId);

        AddRoleItemForm form = new AddRoleItemForm(taskUserDao.findAll(), role);

        model.addAttribute("title", "Add item to role: " + role.getName());
        model.addAttribute("form", form);

        return "role/add-item";
    }

    @RequestMapping(value = "add-item", method = RequestMethod.POST)
    public String addItem(Model model, @ModelAttribute @Valid AddRoleItemForm form,
                          Errors errors) {

        if (errors.hasErrors()) {
            model.addAttribute("form", form);
            return "role/add-item";
        }


        TaskUser taskUser = taskUserDao.findOne(form.getTaskUserId());
        Role role = roleDao.findOne(form.getRoleId());
        role.addItem(taskUser);
        roleDao.save(role);

        return "redirect:/role/view/" + role.getId();
    }
}