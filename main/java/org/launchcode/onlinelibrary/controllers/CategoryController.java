package org.launchcode.onlinelibrary.controllers;

import org.launchcode.onlinelibrary.models.Book;
import org.launchcode.onlinelibrary.models.Category;
import org.launchcode.onlinelibrary.models.data.CategoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("category")
public class CategoryController {

    @Autowired
    private CategoryDao categoryDao;


    @RequestMapping(value = "")
    public String index(Model model) {

        model.addAttribute("categories", categoryDao.findAll());
        model.addAttribute("title", "Categories");

        return "category/index";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(Model model) {

        model.addAttribute("title", "Category");
        model.addAttribute("category", new Category());
        return "category/add";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String add(Model model,@ModelAttribute @Valid Category category, Errors errors) {

        if (errors.hasErrors()) {
            return "category/add";
        }
        categoryDao.save(category);
        return "redirect:";
    }

    @RequestMapping(value = "remove", method = RequestMethod.GET)
    public String displayRemoveCategoryForm(Model model){
        model.addAttribute("categories", categoryDao.findAll());
        model.addAttribute("title","Remove Category");
        return "category/remove";
    }

    @RequestMapping(value = "remove", method = RequestMethod.POST)
    public String processRemoveCategoryForm(@RequestParam(required = false) int[] ids) {

        if(ids == null){
            return "redirect:/category/remove";
        }

        for (int id : ids) {
            categoryDao.delete(id);
        }
        return "redirect:";
    }

    @RequestMapping(value = "edit/{categoryId}", method = RequestMethod.GET)
    public String displayEditCategoryForm(Model model, @PathVariable int categoryId) {

        model.addAttribute("title", "Edit Category");
        model.addAttribute("category",categoryDao.findOne(categoryId));
        model.addAttribute("categories", categoryDao.findAll());
        return "category/edit";
    }

    @RequestMapping(value = "edit/{categoryId}", method = RequestMethod.POST)
    public String processEditCategoryForm(Model model, @PathVariable int categoryId,
                                  @ModelAttribute  @Valid Category newCategory,
                                  Errors errors) {

        if (errors.hasErrors()) {
            model.addAttribute("title", "Add Book");
            return "category/edit";
        }

        Category category = categoryDao.findOne(categoryId);
        category.setName(newCategory.getName());
        categoryDao.save(category);

        return "redirect:/category";
    }
}