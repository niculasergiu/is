package org.launchcode.onlinelibrary.controllers;


import org.launchcode.onlinelibrary.models.Category;
import org.launchcode.onlinelibrary.models.Role;
import org.launchcode.onlinelibrary.models.User;
import org.launchcode.onlinelibrary.models.data.RoleDao;
import org.launchcode.onlinelibrary.models.data.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.MessageDigest;
import java.util.List;

@Controller
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @RequestMapping(value = "")
    public String index(Model model){

        model.addAttribute("users", userDao.findAll());
        model.addAttribute("title","My User");
        return "user/index";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String displayAddUserForm(Model model){
        model.addAttribute("title","Add User");
        model.addAttribute(new User());
        model.addAttribute("roles", roleDao.findAll());
        return "user/add";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String processAddUserForm(@ModelAttribute @Valid User newUser,
                                     Errors errors,@RequestParam int roleId, Model model){

        if(errors.hasErrors()){
            model.addAttribute("title","Add User");
            model.addAttribute("roles",roleDao.findAll());
            return "user/add";
        }

        Iterable<User> users = userDao.findAll();
        for (User user : users){
            if(user.getEmail().equals(newUser.getEmail())) {
                model.addAttribute("title", "Email exist");
                return "user/add";
            }
        }
        Role role = roleDao.findOne(roleId);
        newUser.setRole(role);
        newUser.setPassword(encodePassword(newUser.getPassword()+newUser.getName()));
        userDao.save(newUser);
        return "redirect:";
    }

    @RequestMapping(value = "remove", method = RequestMethod.GET)
    public String displayRemoveUserForm(Model model){
        model.addAttribute("users", userDao.findAll());
        model.addAttribute("title","Remove User");
        return "user/remove";
    }

    @RequestMapping(value = "remove", method = RequestMethod.POST)
    public String processRemoveUserForm(@RequestParam(required = false)  int[] ids) {

        if(ids == null){
            return "redirect:/user/remove";
        }
        for (int id : ids) {
            userDao.delete(id);
        }
        return "redirect:";
    }

    @RequestMapping(value ="roles", method = RequestMethod.GET)
    public String role(Model model, @RequestParam int id){

        Role role = roleDao.findOne(id);
        List<User> users = role.getUsers();
        model.addAttribute("users", users);
        model.addAttribute("title", "User in Role: " + role.getName());
        return "user/index";
    }

    @RequestMapping(value = "edit/{userId}", method = RequestMethod.GET)
    public String displayEditUserForm(Model model, @PathVariable int userId) {

        model.addAttribute("title", "Edit User");
        model.addAttribute("user", userDao.findOne(userId));
        model.addAttribute("roles", roleDao.findAll());
        return "user/edit";
    }

    @RequestMapping(value = "edit/{userId}", method = RequestMethod.POST)
    public String processEditForm(Model model, @PathVariable int userId,
                                  @ModelAttribute  @Valid User newUser,
                                  @RequestParam int roleId,
                                  Errors errors) {

        if (errors.hasErrors()) {
            model.addAttribute("title", "Add User");
            return "user/edit";
        }

        User editedUser = userDao.findOne(userId);
        editedUser.setName(newUser.getName());
        editedUser.setEmail(newUser.getEmail());
        editedUser.setRole(roleDao.findOne(roleId));
        userDao.save(editedUser);

        return "redirect:/user";
    }

    private String encodePassword(String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(password.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

}
