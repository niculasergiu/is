package org.launchcode.onlinelibrary.controllers;


import org.launchcode.onlinelibrary.models.TaskUser;
import org.launchcode.onlinelibrary.models.data.TaskUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("taskUser")
public class TaskUserController {

    @Autowired
    private TaskUserDao taskUserDao;


    @RequestMapping(value = "")
    public String index(Model model) {

        model.addAttribute("taskUsers", taskUserDao.findAll());
        model.addAttribute("title", "TaskUser");

        return "taskUser/index";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(Model model) {

        model.addAttribute("title", "TaskUser");
        model.addAttribute("taskUser", new TaskUser());
        return "taskUser/add";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String add(Model model, @ModelAttribute @Valid TaskUser taskUser, Errors errors) {

        if (errors.hasErrors()) {
            return "taskUser/add";
        }
        taskUserDao.save(taskUser);
        return "redirect:";
    }

    @RequestMapping(value = "remove", method = RequestMethod.GET)
    public String displayRemoveTaskUserForm(Model model){
        model.addAttribute("taskUsers", taskUserDao.findAll());
        model.addAttribute("title","Remove taskUser");
        return "taskUser/remove";
    }

    @RequestMapping(value = "remove", method = RequestMethod.POST)
    public String processRemoveTaskUserForm(@RequestParam(required = false) int[] ids) {

        if(ids == null){
            return "redirect:/taskUser/remove";
        }

        for (int id : ids) {
            taskUserDao.delete(id);
        }
        return "redirect:";
    }

    @RequestMapping(value = "edit/{taskUserId}", method = RequestMethod.GET)
    public String displayEditTaskUserForm(Model model, @PathVariable int taskUserId) {

        model.addAttribute("title", "Edit TaskUser");
        model.addAttribute("taskUser",taskUserDao.findOne(taskUserId));
        model.addAttribute("taskUsers", taskUserDao.findAll());
        return "taskUser/edit";
    }

    @RequestMapping(value = "edit/{taskUserId}", method = RequestMethod.POST)
    public String processEditTaskUserForm(Model model, @PathVariable int taskUserId,
                                      @ModelAttribute  @Valid TaskUser newTaskUser,
                                      Errors errors) {

        if (errors.hasErrors()) {
            model.addAttribute("title", "Add TaskUser");
            return "taskUser/edit";
        }

        TaskUser taskUser = taskUserDao.findOne(taskUserId);
        taskUser.setName(newTaskUser.getName());
        taskUserDao.save(taskUser);

        return "redirect:/taskUser";
    }
}