package org.launchcode.onlinelibrary.controllers;

import org.launchcode.onlinelibrary.models.Role;
import org.launchcode.onlinelibrary.models.User;
import org.launchcode.onlinelibrary.models.data.RoleDao;
import org.launchcode.onlinelibrary.models.data.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.MessageDigest;

@Controller
@RequestMapping("start")
public class StartController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    private User userLogin = new User();

    @RequestMapping(value = "")
    public String index() {
        userLogin = new User();
        return "start/index";
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String displayLoginUserForm(Model model){
        model.addAttribute("title","Login User");
        model.addAttribute(new User());
        model.addAttribute("roles", roleDao.findAll());
        return "start/login";
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String processLoginUserForm(@ModelAttribute @Valid User loginUser,
                                          Errors errors, Model model){

        Iterable<User> users = userDao.findAll();
        for (User user : users){
            if(user.getName().equals(loginUser.getName())
                    && user.getPassword().equals(encodePassword(loginUser.getPassword()+loginUser.getName()))
                    && user.getEmail().equals(loginUser.getEmail())) {
                userLogin.setId(user.getId());
                userLogin.setName(user.getName());
                userLogin.setPassword((user.getPassword()));
                userLogin.setRole(user.getRole());
                userLogin.setEmail(user.getEmail());
                if(user.getRole().getName().equals("admin")){
                    return "redirect:/start/menu";
                }
                else
                {
                    return "redirect:/start/menu";
                }
            }
        }
        model.addAttribute("title", "Nume sau parola incorecta");
        return "start/login";

    }




    @RequestMapping(value = "register", method = RequestMethod.GET)
    public String displayRegisterUserForm(Model model){
        model.addAttribute("title","Add User");
        model.addAttribute(new User());
        model.addAttribute("roles", roleDao.findAll());
        return "start/register";
    }

    @RequestMapping(value = "register", method = RequestMethod.POST)
    public String processRegisterUserForm(@ModelAttribute @Valid User newUser,
                                     Errors errors, Model model){

        if(errors.hasErrors()){
            model.addAttribute("title","Add User");
            model.addAttribute("roles",roleDao.findAll());
            return "start/register";
        }

        Iterable<User> users = userDao.findAll();
        for (User user : users){
            if(user.getEmail().equals(newUser.getEmail())) {
                model.addAttribute("title", "Email exist");
                return "start/register";
            }
        }
        Role role = roleDao.findOne(1);
        newUser.setRole(role);
        newUser.setPassword(encodePassword(newUser.getPassword()+newUser.getName()));
        userDao.save(newUser);
        return "redirect:";
    }

    private String encodePassword(String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(password.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @RequestMapping(value = "menu", method = RequestMethod.GET)
    public String admin(Model model) {
        model.addAttribute("title","Name "+userLogin.getRole().getName() +": "+ userLogin.getName());
        if(userLogin.getRole().getName().equals("admin")){
            return "start/menuAdmin";
        }
        else
        {
            return "start/menuUser";
        }
    }

    @RequestMapping(value = "change", method = RequestMethod.GET)
    public String displayEditUserForm(Model model) {

        model.addAttribute("title", "Change Password");
        model.addAttribute("user", userDao.findOne(userLogin.getId()));
        return "start/change";
    }

    @RequestMapping(value = "change", method = RequestMethod.POST)
    public String processEditForm(Model model,@ModelAttribute  @Valid User newUser,
                                  Errors errors) {

        if (errors.hasErrors()) {
            model.addAttribute("title", "Change Password");
            return "start/change";
        }

        User editedUser = userDao.findOne(userLogin.getId());
        editedUser.setPassword(encodePassword(newUser.getPassword()+userLogin.getName()));
        userDao.save(editedUser);
        return "redirect:/start/menu";
    }
}
