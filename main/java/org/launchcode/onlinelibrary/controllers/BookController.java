package org.launchcode.onlinelibrary.controllers;

import org.launchcode.onlinelibrary.models.Book;
import org.launchcode.onlinelibrary.models.Category;

import org.launchcode.onlinelibrary.models.data.BookDao;
import org.launchcode.onlinelibrary.models.data.CategoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("book")
public class BookController {

    @Autowired
    private BookDao bookeDao;

    @Autowired
    private CategoryDao categoryDao;

    @RequestMapping(value = "")
    public String index(Model model){

        model.addAttribute("books", bookeDao.findAll());
        model.addAttribute("title","My Book");
        return "book/index";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String displayAddBookForm(Model model){
        model.addAttribute("title","Add Book");
        model.addAttribute(new Book());
        model.addAttribute("categories", categoryDao.findAll());
        return "book/add";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String processAddBookForm(@ModelAttribute @Valid Book newBook,
                                       Errors errors,@RequestParam int categoryId , Model model){

        if(errors.hasErrors()){
            model.addAttribute("title","Add Book");
            model.addAttribute("categories",categoryDao.findAll());
            return "book/add";
        }

        Category cat = categoryDao.findOne(categoryId);
        newBook.setCategory(cat);
        bookeDao.save(newBook);
        return "redirect:";
    }

    @RequestMapping(value = "remove", method = RequestMethod.GET)
    public String displayRemoveBookForm(Model model){
        model.addAttribute("books", bookeDao.findAll());
        model.addAttribute("title","Remove Book");
        return "book/remove";
    }

    @RequestMapping(value = "remove", method = RequestMethod.POST)
    public String processRemoveBookForm(@RequestParam(required = false) int[] ids) {

        if(ids == null){
            return "redirect:/book/remove";
        }

        for (int id : ids) {
            bookeDao.delete(id);
        }
        return "redirect:";
    }

    @RequestMapping(value ="category", method = RequestMethod.GET)
    public String category(Model model, @RequestParam int id){

        Category cat = categoryDao.findOne(id);
        List<Book> books = cat.getBooks();
        model.addAttribute("books", books);
        model.addAttribute("title", "Books in Category: " + cat.getName());
        return "book/index";
    }

    @RequestMapping(value = "edit/{bookId}", method = RequestMethod.GET)
    public String displayEditBookForm(Model model, @PathVariable int bookId) {

        model.addAttribute("title", "Edit Book");
        model.addAttribute("book", bookeDao.findOne(bookId));
        model.addAttribute("categories", categoryDao.findAll());
        return "book/edit";
    }

    @RequestMapping(value = "edit/{bookId}", method = RequestMethod.POST)
    public String processEditForm(Model model, @PathVariable int bookId,
                                  @ModelAttribute  @Valid Book newBook,
                                  @RequestParam int categoryId,
                                  Errors errors) {

        if (errors.hasErrors()) {
            model.addAttribute("title", "Add Book");
            return "book/edit";
        }

        Book editedBook = bookeDao.findOne(bookId);
        editedBook.setName(newBook.getName());
        editedBook.setAuthor(newBook.getAuthor());
        editedBook.setYear(newBook.getYear());
        editedBook.setDescription(newBook.getDescription());
        editedBook.setCategory(categoryDao.findOne(categoryId));
        bookeDao.save(editedBook);

        return "redirect:/book";
    }

}
